tee output_create_temp.txt

-- library collection inventory table
DROP TABLE IF EXISTS Library_Collection_Inventory;
CREATE TABLE Library_Collection_Inventory (
    BibNum              int,
    Title               text,
    Author              text,
    ISBN                text,
    PublicationYear     text,
    Publisher           text,
    Subjects            text,
    ItemType            varchar(10),
    ItemCollection      varchar(7),
    FloatingItem        varchar(8),
    ItemLocation        varchar(4),
    ReportDate          varchar(10),
    ItemCount           int
);

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Library_Collection_Inventory.csv' IGNORE
INTO TABLE Library_Collection_Inventory
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 2677150 LINES;

-- book table
DROP TABLE IF EXISTS Book;
CREATE TABLE Book (
    Id                  int,
    Name                varchar(511),
    pagesNumber         int,
    Rating              varchar(20),
    RatingDist1         varchar(20),
    RatingDist2         varchar(20),
    RatingDist3         varchar(20),
    RatingDist4         varchar(20),
    RatingDist5         varchar(20),
    RatingDistTotal     varchar(20),
    PublishDay          int,
    PublishMonth        int,
    PublishYear         int,
    Publisher           varchar(511),
    Language            varchar(10),
    Authors             varchar(511),
    ISBN                varchar(10),
    Description         text,
    CountsOfReview      int,
    Count_Text_Review   int             -- newly created column: corresponds to Counts of text review
);    

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1-100k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 57293 LINES
(@Id, @Name, @RatingDist1, @pagesNumber, @RatingDist4, @RatingDistTotal, @PublishMonth, @PublishDay, @Publisher, @CountsOfReview, @PublishYear, @Language, @Authors, @Rating, @RatingDist2, @RatingDist5, @ISBN, @RatingDist3)
set Id = @Id,
    Name = @Name, 
    RatingDist1 = @RatingDist1, 
    pagesNumber = @pagesNumber, 
    RatingDist4 = @RatingDist4, 
    RatingDistTotal = @RatingDistTotal,
    PublishMonth = @PublishDay, 
    PublishDay = @PublishMonth, 
    Publisher = @Publisher, 
    CountsOfReview = @CountsOfReview, 
    PublishYear = @PublishYear, 
    Language = @Language, 
    Authors = @Authors, 
    Rating = @Rating, 
    RatingDist2 = @RatingDist2, 
    RatingDist5 = @RatingDist5, 
    ISBN = @ISBN, 
    RatingDist3 = @RatingDist3;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book100k-200k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 56047 LINES
(@pagesNumber, @Authors, @Publisher, @Rating, @Language, @RatingDistTotal, @RatingDist5, @RatingDist3, @CountsOfReview, @PublishDay, @ISBN, @RatingDist4, @PublishMonth, @Id, @PublishYear, @RatingDist1, @RatingDist2, @Name)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay, 
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    CountsOfReview = @CountsOfReview;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book200k-300k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 55183 LINES
(@Publisher, @RatingDistTotal, @RatingDist5, @PublishDay, @Name, @Rating, @pagesNumber, @Language, @PublishMonth, @Id, @RatingDist4, @RatingDist1, @ISBN, @RatingDist2, @CountsOfReview, @Authors, @RatingDist3, @PublishYear)
set Id = @Id,
    Name = @Name, 
    RatingDist1 = @RatingDist1, 
    pagesNumber = @pagesNumber, 
    RatingDist4 = @RatingDist4, 
    RatingDistTotal = @RatingDistTotal,
    PublishMonth = @PublishDay,  
    PublishDay = @PublishMonth, 
    Publisher = @Publisher, 
    CountsOfReview = @CountsOfReview, 
    PublishYear = @PublishYear, 
    Language = @Language, 
    Authors = @Authors, 
    Rating = @Rating, 
    RatingDist2 = @RatingDist2, 
    RatingDist5 = @RatingDist5, 
    ISBN = @ISBN, 
    RatingDist3 = @RatingDist3;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book300k-400k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 55587 LINES
(@RatingDist4, @RatingDist1,@ISBN,@Authors,@Id,@pagesNumber,@Language,@RatingDist3,@Name,@PublishYear,@CountsOfReview,@RatingDist5,@PublishMonth,@RatingDist2,@PublishDay,@RatingDistTotal,@Rating,@Publisher)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    CountsOfReview = @CountsOfReview;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book400k-500k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 54156 LINES
(@PublishYear,@Rating,@RatingDistTotal,@ISBN,@RatingDist1,@Publisher,@PublishMonth,@Id,@Name,@Authors,@RatingDist5,@RatingDist4,@PublishDay,@RatingDist2,@pagesNumber,@RatingDist3,@CountsOfReview,@Language)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    CountsOfReview = @CountsOfReview;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book500k-600k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 53860 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    CountsOfReview = @CountsOfReview;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book600k-700k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
ESCAPED BY '<'
LINES TERMINATED BY '\n'
IGNORE 55844 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book700k-800k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 54915 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description,@Count_Text_Review)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book800k-900k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 50485 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description,@Count_Text_Review)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book900k-1000k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 41167 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description,@Count_Text_Review)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1000k-1100k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 40070 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description,@Count_Text_Review)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1100k-1200k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 42303 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description,@Count_Text_Review)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1200k-1300k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 43803 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description,@Count_Text_Review)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1300k-1400k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 38326 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description,@Count_Text_Review)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1400k-1500k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 34814 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description,@Count_Text_Review)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1500k-1600k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 33477 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description,@Count_Text_Review)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1600k-1700k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 32870 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description,@Count_Text_Review)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1700k-1800k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 32043 LINES
(@Authors,@CountsOfReview,@Description,@ISBN,@Id,@Language,@Name,@PublishDay,@PublishMonth,@PublishYear,@Publisher,@Rating,@RatingDist1,@RatingDist2,@RatingDist3,@RatingDist4,@RatingDist5,@RatingDistTotal,@pagesNumber)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishMonth, 
    PublishMonth = @PublishDay,  
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1800k-1900k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 38752 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishDay, 
    PublishMonth = @PublishMonth, 
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book1900k-2000k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 43483 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishDay, 
    PublishMonth = @PublishMonth, 
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview, 
    Count_Text_Review = @Count_Text_Review;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book2000k-3000k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
ESCAPED BY "<"
LINES TERMINATED BY '\n'
IGNORE 404545 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishDay, 
    PublishMonth = @PublishMonth, 
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book3000k-4000k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
ESCAPED BY "<"
LINES TERMINATED BY '\n'
IGNORE 260770 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishDay, 
    PublishMonth = @PublishMonth, 
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/book4000k-5000k.csv' IGNORE
INTO TABLE Book
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
ESCAPED BY "<"
LINES TERMINATED BY '\n'
IGNORE 283423 LINES
(@Id,@Name,@Authors,@ISBN,@Rating,@PublishYear,@PublishMonth,@PublishDay,@Publisher,@RatingDist5,@RatingDist4,@RatingDist3,@RatingDist2,@RatingDist1,@RatingDistTotal,@CountsOfReview,@Language,@pagesNumber,@Description)
set Id = @Id,
    Name = @Name, 
    pagesNumber = @pagesNumber,
    Rating = @Rating,
    RatingDist1 = @RatingDist1,
    RatingDist2 = @RatingDist2, 
    RatingDist3 = @RatingDist3, 
    RatingDist4 = @RatingDist4,
    RatingDist5 = @RatingDist5, 
    RatingDistTotal = @RatingDistTotal,
    PublishDay = @PublishDay, 
    PublishMonth = @PublishMonth, 
    PublishYear = @PublishYear, 
    Publisher = @Publisher, 
    Language = @Language, 
    Authors = @Authors, 
    ISBN = @ISBN,
    Description = @Description,
    CountsOfReview = @CountsOfReview;

notee;