-- Do this AFTER running both create_rs.sql and create_tables.py
-- Removes the tables created by create_temp.sql

DROP TABLE IF EXISTS Library_Collection_Inventory;
DROP TABLE IF EXISTS Book;
DROP TABLE IF EXISTS User_Rating;