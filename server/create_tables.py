import mysql.connector as mysql

db = mysql.connect(
    host = "marmoset04.shoshin.uwaterloo.ca",
    user = "userid",
    passwd = "password",
    db = "db356_userid"
)

## creating an instance of 'cursor' class which is used to execute the 'SQL' statements in 'Python'
cursor = db.cursor()


####### Library_Goodreads ########

# CREATE TABLE Library_Goodreads (
#     BibNum              int,
#     ISBN                varchar(15),    -- Might need to change this to varchar(15-ish) for Library_Inventory ISBNs
#     primary key (BibNum, ISBN)
#     -- foreign key (BibNum) references Library_Inventory(BibNum)
# );
cursor.execute("DROP TABLE IF EXISTS Library_Goodreads")

cursor.execute("CREATE TABLE Library_Goodreads (BibNum int, ISBN varchar(15), primary key (BibNum, ISBN), foreign key (BibNum) references Library_Inventory(BibNum))")

cursor.execute("SELECT BibNum, ISBN FROM Library_Collection_Inventory WHERE ISBN IS NOT NULL and length(ISBN) > 0 LIMIT 1000")

lib_gr_records = cursor.fetchall()

for record in lib_gr_records:
    isbns = record[1].split(", ")
    insert_query = "INSERT IGNORE INTO Library_Goodreads(BibNum, ISBN) VALUES (%s, %s)"
    args = []
    for isbn in isbns:
        args.append((record[0], isbn))
    cursor.executemany(insert_query, args)

# cursor.execute("ALTER TABLE Library_Inventory DROP COLUMN ISBN")

####### Subjects ########
# CREATE TABLE Subjects (
#     BibNum              int,
#     Subject             varchar(511),   -- Originally text but text cannot be used as a primary key
#     primary key (BibNum, Subject),
#     foreign key (BibNum) references Library_Inventory(BibNum)
# );

cursor.execute("DROP TABLE IF EXISTS Subjects")

cursor.execute("CREATE TABLE Subjects (BibNum int, Subject varchar(511), primary key (BibNum, Subject), foreign key (BibNum) references Library_Inventory(BibNum))")

cursor.execute("SELECT BibNum, Subjects from Library_Collection_Inventory WHERE Subjects IS NOT NULL and length(Subjects) > 0 LIMIT 1000")

subject_records = cursor.fetchall()

for record in subject_records:
    subjects = record[1].split(", ")
    insert_query = "INSERT IGNORE INTO Subjects(BibNum, Subject) VALUES (%s, %s)"
    args = []
    for subject in subjects:
        args.append((record[0], subject))
    cursor.executemany(insert_query, args)

db.commit()
