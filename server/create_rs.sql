tee output_create_rs.txt

-- Drop tables in order which does not cause errors due to foreign key constraints
DROP TABLE IF EXISTS Library_Goodreads;

DROP TABLE IF EXISTS Checkouts;
DROP TABLE IF EXISTS Subjects;
DROP TABLE IF EXISTS Library_Inventory;

DROP TABLE IF EXISTS Format;
DROP TABLE IF EXISTS Category;
DROP TABLE IF EXISTS Data_Dictionary;

DROP TABLE IF EXISTS User_Ratings;
DROP TABLE IF EXISTS User_Rating;
DROP TABLE IF EXISTS Publish_Info;
DROP TABLE IF EXISTS Languages;
DROP TABLE IF EXISTS Authors;
DROP TABLE IF EXISTS Ratings;
DROP TABLE IF EXISTS Books;

-- Data_Dictionary table
CREATE TABLE Data_Dictionary (
    Code                varchar(7)      primary key,
    Description         text,
    CodeType            varchar(14)
);

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Integrated_Library_System__ILS__Data_Dictionary.csv' IGNORE
INTO TABLE Data_Dictionary
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(@Code, @Description, @CodeType, @dummy, @dummy, @dummy, @dummy)
set Code = @Code,
    Description = @Description,
    CodeType = @CodeType;

-- Format table
CREATE TABLE Format (
    Code                varchar(7)      primary key,
    FormatGroup         varchar(9),
    FormatSubgroup      varchar(14),
    foreign key (Code) references Data_Dictionary(Code)
);

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Integrated_Library_System__ILS__Data_Dictionary.csv' IGNORE
INTO TABLE Format
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(@Code, @dummy, @dummy, @FormatGroup, @FormatSubgroup, @dummy, @dummy)
set Code = @Code,
    FormatGroup = NULLIF(@FormatGroup, ''),
    FormatSubGroup = NULLIF(@FormatSubGroup, '');

DELETE FROM Format WHERE FormatGroup IS NULL AND FormatSubGroup IS NULL;

-- Category table
CREATE TABLE Category (
    Code                varchar(7)      primary key,
    CategoryGroup       varchar(10),
    CategorySubgroup    varchar(11),
    foreign key (Code) references Data_Dictionary(Code)
);

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Integrated_Library_System__ILS__Data_Dictionary.csv' IGNORE
INTO TABLE Category
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(@Code, @dummy, @dummy, @dummy, @dummy, @CategoryGroup, @CategorySubgroup)
set Code = @Code,
    CategoryGroup = NULLIF(@CategoryGroup, ''),
    CategorySubgroup = NULLIF(@CategorySubgroup, '');

DELETE FROM Category WHERE CategoryGroup IS NULL AND CategorySubGroup IS NULL;

-- Library_Inventory table
CREATE TABLE Library_Inventory (
    BibNum              int,
    Title               text,
    ItemType            varchar(7),
    ItemCollection      varchar(7),
    ItemFloating        boolean,
    ItemLocation        varchar(4),
    ItemCount           int         not null,
    primary key (BibNum, ItemType, ItemCollection, ItemLocation),
    foreign key (ItemType) references Data_Dictionary(Code),
    foreign key (ItemCollection) references Data_Dictionary(Code),
    foreign key (ItemLocation) references Data_Dictionary(Code)
);

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Library_Collection_Inventory.csv'
IGNORE INTO TABLE Library_Inventory
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 2677150 LINES
(@BibNum, @Title, @Author, @ISBN, @PublicationYear, @Publisher, @Subjects, @ItemType, @ItemCollection, @FloatingItem, @ItemLocation, @ReportDate, @ItemCount)
set BibNum = @BibNum,
    Title = NULLIF(@Title, ''),
    ItemType = @ItemType,
    ItemCollection = @ItemCollection,
    ItemFloating = 
        CASE
            WHEN UPPER(@FloatingItem) = 'FLOATING' THEN TRUE
            ELSE FALSE
        END,
    ItemLocation = @ItemLocation,
    ItemCount = @ItemCount;

-- Checkouts table
CREATE TABLE Checkouts (
    BibNum              int, 
    CheckoutDateTime    dateTime,
    CallNumber          text,
    ItemBarcode         decimal(13,0),
    ItemType            varchar(7),
    ItemCollection      varchar(7),
    primary key (BibNum, CheckoutDateTime),
    foreign key (BibNum) references Library_Inventory(BibNum),
    foreign key (ItemType) references Data_Dictionary(Code),
    foreign key (ItemCollection) references Data_Dictionary(Code)
);

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2005.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
ESCAPED BY ''
LINES TERMINATED BY '\n'
IGNORE 3793727 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');


LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2006.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
ESCAPED BY '<'
LINES TERMINATED BY '\n'
IGNORE 6581480 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');


LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2007.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
ESCAPED BY '<'
LINES TERMINATED BY '\n'
IGNORE 7091008 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');


LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2008.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
ESCAPED BY '<'
LINES TERMINATED BY '\n'
IGNORE 8373542 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');


LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2009.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
ESCAPED BY '<'
LINES TERMINATED BY '\n'
IGNORE 9034839 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');


LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2010.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 8424047 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2011.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 7783796 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2012.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 7298125 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2013.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 7863261 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2014.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 7415880 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2015.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 6870627 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2016.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 6403831 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/Checkouts_By_Title_Data_Lens_2017.csv'
IGNORE INTO TABLE Checkouts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 5033543 LINES
(@BibNumber, @ItemBarcode, @ItemType, @Collection, @CallNumber, @CheckoutDateTime)
set BibNum = @BibNumber,
    CheckoutDateTime = CONCAT(REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 3), 
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 1),
                                '-',
                                REGEXP_SUBSTR(@CheckoutDateTime, '[0-9]+', 1, 2),
                                SUBSTR(@CheckoutDateTime, 11, 9)),
    CallNumber =   NULLIF(@CallNumber, ''),
    ItemBarcode = NULLIF(@ItemBarcode, ''),
    ItemType = NULLIF(@ItemType, ''),
    ItemCollection = NULLIF(@ItemCollection, '');

-- Books table
CREATE TABLE Books (
    Id                  int             primary key,
    Name                varchar(511)    unique,
    pagesNumber         int,
    PublishDate         dateTime,
    Description         text,
    CountsOfReview      int,
    ISBN                varchar(10)
);

INSERT IGNORE INTO Books (Id, Name, pagesNumber, PublishDate, Description, CountsOfReview, ISBN)
SELECT Id,
    Name,
    pagesNumber,
    concat(PublishYear, '-', PublishMonth, '-', PublishDay) as PublishDate,
    Description,
    CountsOfReview,
    ISBN
    from Book;

-- User_Ratings table 
-- First create User_Rating from original csvs and then join
CREATE TABLE User_Rating (
    User_id             int,
    Name                varchar(511),
    Rating              text
);

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/user_rating_0_to_1000.csv' IGNORE
INTO TABLE User_Rating
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 50946 LINES;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/user_rating_1000_to_2000.csv' IGNORE
INTO TABLE User_Rating
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 41987 LINES;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/user_rating_2000_to_3000.csv' IGNORE
INTO TABLE User_Rating
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 29634 LINES;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/user_rating_3000_to_4000.csv' IGNORE
INTO TABLE User_Rating
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 45971 LINES;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/user_rating_4000_to_5000.csv' IGNORE
INTO TABLE User_Rating
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 45904 LINES;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/user_rating_5000_to_6000.csv' IGNORE
INTO TABLE User_Rating
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 14482 LINES;

LOAD DATA INFILE '/var/lib/mysql-files/20-Books/user_rating_6000_to_11000.csv' IGNORE
INTO TABLE User_Rating
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 126679 LINES;

CREATE TABLE User_Ratings (
    Id                  int,
    User_id             int,
    Rating              varchar(35),
    primary key (Id, User_id),
    foreign key (Id) references Books(Id)
);

INSERT IGNORE INTO User_Ratings SELECT Id,
                                    User_id,
                                    Rating
                                FROM User_Rating INNER JOIN Books USING (Name) LIMIT 1000;

-- Publish_Info table 
CREATE TABLE Publish_Info (
    Id                  int,
    Publisher           varchar(511),
    primary key (Id, Publisher),
    foreign key (Id) references Books(Id)
);

INSERT IGNORE INTO Publish_Info (Id, Publisher)
SELECT Id, Publisher from Book LIMIT 1000;

-- Languages table 
CREATE TABLE Languages (
    Id                  int,
    Language            varchar(10),
    primary key (Id, Language),
    foreign key (Id) references Books(Id)
);

INSERT IGNORE INTO Languages (Id, Language)
SELECT Id, Language from Book where length(Language) > 0 LIMIT 1000;

-- Authors table 
CREATE TABLE Authors (
    Id                  int,
    Author              varchar(511),
    primary key (Id, Author),
    foreign key (Id) references Books(Id)
);

INSERT IGNORE INTO Authors (Id, Author)
SELECT Id, Authors from Book LIMIT 1000;

-- Ratings table 
CREATE TABLE Ratings (
    Id                  int     primary key,
    Rating              decimal(3,2),
    -- The following attributes have been changed from varchar(20) to int
    -- The data will need to be inserted using select insert statements
    Rating1             int,
    Rating2             int,
    Rating3             int,
    Rating4             int,
    Rating5             int,
    RatingTotal         int,
    foreign key (Id) references Books(Id)
);

INSERT IGNORE INTO Ratings SELECT Id,
                            SUBSTR(Rating, 1, 3) AS Rating,
                            REGEXP_SUBSTR(RatingDist1, '[0-9]+', 1, 2) AS Rating1,
                            REGEXP_SUBSTR(RatingDist2, '[0-9]+', 1, 2) AS Rating2,
                            REGEXP_SUBSTR(RatingDist3, '[0-9]+', 1, 2) AS Rating3,
                            REGEXP_SUBSTR(RatingDist4, '[0-9]+', 1, 2) AS Rating4,
                            REGEXP_SUBSTR(RatingDist5, '[0-9]+', 1, 2) AS Rating5,
                            REGEXP_SUBSTR(RatingDistTotal, '[0-9]+', 1, 1) AS RatingTotal
                        FROM Book LIMIT 1000;

notee;