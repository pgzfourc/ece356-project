import mysql.connector as mysql
from datetime import datetime
from sys import argv
import shlex
import logging

logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger(__name__)

db = mysql.connect(
    host = "marmoset04.shoshin.uwaterloo.ca",
    user = "userid",
    passwd = "pw",
    db = "db356_userid"
)

## creating an instance of 'cursor' class which is used to execute the 'SQL' statements in 'Python'
cursor = db.cursor()

#check if book from goodreads is available in library inventory
def check_book_from_good_reads_in_library_inventory(args):
    if len(args) != 1:
        raise ValueError("Expected 1 argument")
    
    Id = args[0]
    # grab isbn
    cursor.execute(f"Select ISBN from Books where Id={Id}")

    books_res = cursor.fetchall()

    for books_record in books_res:
        ISBN = books_record[0]
        # grab BibNum using ISBN
        cursor.execute(f"Select BibNum from Library_Goodreads where ISBN='{ISBN}'")
        library_good_reads_res = cursor.fetchall()
        for library_good_reads_record in library_good_reads_res:
            BibNum = library_good_reads_record[0]
            # grab ItemCount from record with matching BibNum
            cursor.execute(f"Select ItemCount from Library_Inventory where BibNum={BibNum}")
            library_inventory_res = cursor.fetchall()
            
            # check item availability
            if len(library_inventory_res) == 0:
                logger.info(f"The book with id {Id} is not in our inventory.")
                return - 1
            
            for library_inventory_record in library_inventory_res:
                itemCount = library_inventory_record[0]
                if itemCount > 0:
                    logger.info(f"{itemCount} book(s) with id {Id} is/are available.")
                    return itemCount
            return -1
    logger.info(f"No ISBN found for book with id {Id}.")

#Borrow book by bibNum
def borrow_book_by_bibnum(args):
    if len(args) != 1:
        raise ValueError("Expected 1 argument")
    bibNum = args[0]
    # grab parameters for borrowing book from record with matching bibNum
    cursor.execute(f"Select ItemType, ItemCollection, ItemLocation, ItemCount from Library_Inventory where BibNum={bibNum}")
    inventory_res = cursor.fetchall()

    for inventory_record in inventory_res:
        itemType = inventory_record[0]
        itemCollection = inventory_record[1]
        itemLocation = inventory_record[2]
        itemCount = inventory_record[3]
        # check item availability
        if itemCount > 0:
            #update item count
            cursor.execute(f"Update Library_Inventory SET ItemCount = {itemCount-1} where BibNum={bibNum} and ItemType='{itemType}' and ItemLocation='{itemLocation}' and ItemCollection='{itemCollection}'")
            #grab barcode 
            cursor.execute(f"select ItemBarcode from Checkouts where BibNum={bibNum} and ItemType='{itemType}' and ItemCollection='{itemCollection}'")
            checkouts_res = cursor.fetchall()

            itemBarcode = 10000000000
            for checkout_record in checkouts_res:
                itemBarcode = checkout_record[0]
                break
            # add record to checkout table
            cursor.execute(f"INSERT INTO Checkouts VALUES ({bibNum}, '{datetime.now()}', '', {itemBarcode}, '{itemType}', '{itemCollection}')")   
        else:
            logger.info('Sorry this book is not available to borrow')
            return -1
        break
    else:
        logger.info('Sorry this book is not in the library\'s inventory')
        return -2


# User wants to borrow a book:
def borrow_book_by_title(args):
    if len(args) != 1:
        raise ValueError("Expected 1 argument")
    title = args[0]
    # grab parameters for borrowing book from record with matching title
    cursor.execute(f"Select BibNum, ItemType, ItemCollection, ItemLocation, ItemCount from Library_Inventory where Title='{title}'")
    inventory_res = cursor.fetchall()

    for inventory_record in inventory_res:
        bibNum = inventory_record[0]
        itemType = inventory_record[1]
        itemCollection = inventory_record[2]
        itemLocation = inventory_record[3]
        itemCount = inventory_record[4]
        
        if itemCount > 0:
            #update item count
            cursor.execute(f"Update Library_Inventory SET ItemCount = {itemCount-1} where BibNum={bibNum} and ItemType='{itemType}' and ItemLocation='{itemLocation}' and ItemCollection='{itemCollection}'")
            #grab barcode 
            cursor.execute(f"select ItemBarcode from Checkouts where BibNum={bibNum} and ItemType='{itemType}' and ItemCollection='{itemCollection}'")
            checkouts_res = cursor.fetchall()

            itemBarcode = 10000000000
            for checkout_record in checkouts_res:
                itemBarcode = checkout_record[0]
                break
            
            # add record to checkout table
            cursor.execute(f"INSERT INTO Checkouts VALUES ({bibNum}, '{datetime.now()}', '', {itemBarcode}, '{itemType}', '{itemCollection}')")
        else:
            logger.info('Sorry this book is not available to borrow')
            return -1
        break
    else:
        logger.info('Sorry this book is not in the library\'s inventory')
        return -2

# Check if book is available to book:
def check_availability(args):
    if len(args) != 1:
        raise ValueError("Expected 1 argument")
    title = args[0]
    # grab ItemCount from library inventory using title
    cursor.execute(f"Select ItemCount from Library_Inventory where Title='{title}'")
    inventory_res = cursor.fetchall()

    if len(inventory_res) == 0:
        logger.info('Sorry this book does not exist in the inventory')
        return
    
    for inventory_record in inventory_res:
        itemCount = inventory_record[0]
        
        # check item availability
        if itemCount > 0:
            logger.info(f"There are {itemCount} copies available to borrow")
            return itemCount
    logger.info('Sorry this book does not exist in the inventory')

#add to Library_Inventory
def add_to_library_inventory(args):
    if len(args) != 7:
        raise ValueError("Expected 7 arguments")
    BibNum = args[0]
    Title = args[1]
    ItemType = args[2]
    ItemCollection = args[3]
    ItemFloating = args[4]
    ItemLocation = args[5]
    ItemCount = args[6]
    try:
        # create record in library inventory using args
        cursor.execute(f"INSERT INTO Library_Inventory VALUES ({BibNum}, '{Title}', '{ItemType}', '{ItemCollection}', {ItemFloating}, '{ItemLocation}', {ItemCount})")
    except:
        raise ValueError("Could not insert data into Library_Inventory")

#remove from Library_Inventory
def remove_from_library_inventory(args):
    if len(args) != 4:
        raise ValueError("Expected 4 arguments")
    BibNum = args[0]
    ItemType = args[1]
    ItemCollection = args[2]
    ItemLocation = args[3]
    try:
        # Need to delete from tables that reference it
        cursor.execute(f"delete from Checkouts where BibNum = {BibNum} and ItemType = '{ItemType}' and ItemCollection = '{ItemCollection}'")
        # Delete record from library inventory using args
        cursor.execute(f"DELETE FROM Library_Inventory WHERE BibNum={BibNum} and ItemType='{ItemType}' and ItemCollection='{ItemCollection}' and ItemLocation='{ItemLocation}'")
    except:
        raise ValueError("Could not delete from Library_Inventory")

#add Books
def add_to_books(args):
    if len(args) != 7:
        raise ValueError("Expected 7 arguments")
    Id = args[0]
    Name = args[1]
    pagesNumber = args[2]
    PublishDate = args[3]
    Description = args[4]
    CountsOfReview = args[5]
    ISBN = args[6]
    try:
        # create record in books using args
        cursor.execute(f"INSERT INTO Books VALUES ({Id}, '{Name}', {pagesNumber}, '{PublishDate}', '{Description}', {CountsOfReview}, '{ISBN}')")
    except:
        raise ValueError("Could not insert data into Books")

#remove from Books
def remove_from_books(args):
    if len(args) != 1:
        raise ValueError("Expected 1 argument")
    Id = args[0]
    try:
        # Need to delete from tables that reference it
        cursor.execute(f"delete from User_Ratings where Id = {Id}")
        cursor.execute(f"delete from Publish_Info where Id = {Id}")
        cursor.execute(f"delete from Languages where Id = {Id}")
        cursor.execute(f"delete from Authors where Id = {Id}")
        cursor.execute(f"delete from Ratings where Id = {Id}")
        # delete record from books using id
        cursor.execute(f"DELETE FROM Books WHERE Id = {Id}")
    except:
        raise ValueError("Could not delete from Books")

#get reviews for book by id
def get_reviews_by_id(args):
    if len(args) != 1:
        raise ValueError("Expected 1 argument")
    Id = args[0]
    try:
        # grab review using id
        cursor.execute(f"Select Rating from User_Ratings where Id={Id}")
        user_ratings_res = cursor.fetchall()
        for record in user_ratings_res:
            logger.info(record[0])
        return user_ratings_res
    except:
        raise ValueError("Could not get reviews for Book")

#get ratings for book by id
def get_ratings_by_id(args):
    if len(args) != 1:
        raise ValueError("Expected 1 argument")
    Id = args[0]
    try:
        # grab rating using id
        cursor.execute(f"Select Rating1, Rating2, Rating3, Rating4, Rating5, RatingTotal, Rating from Ratings where Id='{Id}'")
        user_ratings_res = cursor.fetchall()
        ratings = []
        for record in user_ratings_res:
            logger.info(f"Rating1: {record[0]}, Rating2: {record[1]}, Rating3: {record[2]}, Rating4: {record[3]}, Rating5: {record[4]}, RatingTotal: {record[5]}, Rating: {record[6]}")
            ratings.append(record[6])
        return ratings
    except:
        raise ValueError("Could not get reviews for Book")

#add reviews
def add_review(args):
    if len(args) != 3:
        raise ValueError("Expected 3 arguments")
    Book_Id = args[0]
    User_id = args[1]
    Rating = args[2]
    try:
        # add review using args
        cursor.execute(f"INSERT INTO User_Ratings VALUES ({Book_Id}, {User_id}, '{Rating}')")
    except:
        raise ValueError("Could not insert into User_Ratings")

#update reviews
def update_review(args):
    if len(args) != 3:
        raise ValueError("Expected 3 arguments")
    Book_Id = args[0]
    User_id = args[1]
    Rating = args[2]
    # update review with args
    cursor.execute(f"Update User_Ratings SET Rating = '{Rating}' where Id={Book_Id} and User_id={User_id}")

#get additional info about codes
def get_info_about_code(args):
    if len(args) != 1:
        raise ValueError("Expected 1 argument")
    Code = args[0]
    try:
        # grab all columns from data dictionary using code from args
        cursor.execute(f"Select * from Data_Dictionary INNER JOIN Format using(Code) INNER JOIN Category using(Code) where Code='{Code}'")
        res = cursor.fetchall()
        logger.info(f"About code {Code}:\nCode, Description, Code Type, Format Group, Format Subgroup, Category Group, Category Subgroup\n{res}")
        return res
    except:
        raise ValueError("Could not get info about code")

#get a books subjects
def get_subjects(args):
    if len(args) != 1:
        raise ValueError("Expected 1 argument")
    BibNum = args[0]
    try:
        # grab subject from subjects using bibnum from args
        cursor.execute(f"Select Subject from Subjects where BibNum={BibNum}")
        res = cursor.fetchall()
        logger.info(f"Related subjects: {res}")
        return res
    except:
        raise ValueError("Could not get info about subjects")

#list books by subjects
def list_books_by_subjects(args):
    if len(args) != 1:
        raise ValueError("Expected 1 argument")
    subject = args[0].lower()
    try:
        # grab all books for passed in subject
        cursor.execute(f"Select Subject, Title from Subjects INNER JOIN Library_Inventory using(BibNum) WHERE LOWER(Subject) LIKE '%{subject}%'")
        res = cursor.fetchall()
        logger.info(f"Books related to {subject}:")
        for i in res:
            logger.info(f"{i}")
        return res
    except:
        raise ValueError("Could not get info about subjects")

#count books by subjects
def count_books_by_subjects(args):
    try:
        # count number of books for passed in subject
        cursor.execute(f"Select Subject, COUNT(Title) from Subjects INNER JOIN Library_Inventory using(BibNum) GROUP BY Subject")
        res = cursor.fetchall()
        logger.info(f"Books per subject:")
        for i in res:
            logger.info(f"{i}")
        return res
    except:
        raise ValueError("Could not get info about subjects")


# help command mapping
HELP = {
    "To find a book in library inventory using book id, run": "find book with id <id>",
    "To find a book in library inventory using book title, run": "find book with title <title>",
    "To find the subjects of a book using book bib number, run": "find book subjects <bibnum>",
    "To find reviews for a book using book id, run": "find book reviews with id <id>",
    "To find ratings for a book using book id, run": "find book ratings with id <id>",
    "To find additional details using ItemCollection/ItemType, run": "find additional details <ItemCollection/ItemType>",
    "To borrow book using bib number, run": "borrow book with bibnum <bibnum>",
    "To borrow book using book title, run": "borrow book with title <title>",
    "To add book to library inventory, run": "add to library inventory <BibNum> <Title> <ItemType> <ItemCollection> <ItemFloating> <ItemLocation> <ItemCount>",
    "To add book to books, run": "add to books <Id> <Name> <pagesNumber> <PublishDate> <Description> <CountsOfReview> <ISBN>",
    "To add book review, run": "add review <Book_Id> <User_id> <Rating>",
    "To delete record from library inventory, run": "delete from library inventory <BibNum> <ItemType> <ItemCollection> <ItemLocation>",
    "To delete record from books, run": "delete from books <Id>", 
    "To update review, run": "update review <Book_Id> <User_id> <Rating>",
    "To list books by subject, run": "list books by subject <subject>",
    "To count books by subject, run": "count books by subject <subject>"
}

# command mapping
COMMANDS = {
    "find book with id": check_book_from_good_reads_in_library_inventory,
    "find book with title": check_availability,
    "find book subjects": get_subjects,
    "find book reviews with id": get_reviews_by_id,
    "find book ratings with id": get_ratings_by_id,
    "find additional details": get_info_about_code,
    "borrow book with bibnum": borrow_book_by_bibnum,
    "borrow book with title": borrow_book_by_title,
    "add to library inventory": add_to_library_inventory,
    "add to books": add_to_books,
    "add review": add_review, 
    "delete from library inventory": remove_from_library_inventory,
    "delete from books": remove_from_books,
    "update review": update_review,
    "list books by subject": list_books_by_subjects,
    "count books by subject": count_books_by_subjects
}

# function used for running tests
def run_command(command):
    command = "test " + command
    temp_argv = shlex.split(command)

    command = ""
    args = []
    for i in range(1, len(temp_argv)):
        if i == 1:
            command += temp_argv[i].lower()
            continue
        if command not in COMMANDS:
            command += " " + temp_argv[i].lower()
        else:
            args.append(temp_argv[i])

    ret = COMMANDS[command](args)
    db.commit()
    return ret

# main function
def main():
    # check for help command
    if len(argv) == 2 and argv[1].lower() == "help":
        for key, value in HELP.items():
            print(f"{key}: {value}")
        return

    min_args = 5
    if len(argv) < min_args:
        raise ValueError(f"Need at least {min_args} for a command")

    # parse argv input
    logger.info('Parsing Input')
    command = ""
    args = []
    for i in range(1, len(argv)):
        if i == 1:
            command += argv[i].lower()
            continue
        if command not in COMMANDS:
            command += " " + argv[i].lower()
        else:
            args.append(argv[i])

    # run command and commit to the database
    logger.info(f"Running {command} with args {args}")
    COMMANDS[command](args)

    logger.info(f"Commiting to database and exiting")
    db.commit()

if __name__ == "__main__":
    main()