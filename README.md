# ECE356 Project

# Due Date
December 23, 2021 @ 4:30pm EST

# Youtube link for video
https://youtu.be/OTnrP7a2twU

# How to create the server
Expected runtime: depends on rows ignored/limited, but the current setup should be fast.

1. Type `cd server` and press enter
2. Log in to marmoset04
3. Connect to db356_userid
4. Type `source create_temp.sql` and press enter
5. Type `source create_rs.sql` and press enter
6. In `create_tables.py`, enter your username, password, and db name, then save
7. In terminal (not MySQL) type `python3 create_tables.py` and press enter
8. In MySQL type `source remove_temp.sql` and press enter. In the end, there should be 13 tables.

# How to use the client

1. Open `cli_application.py` in a text editor and add your MySQL credentials near the top of the file where it says
```
db = mysql.connect(
    host = "marmoset04.shoshin.uwaterloo.ca",
    user = "userid",
    passwd = "pw",
    db = "db356_userid"
)
```
3. In the command line type `python3 cli_application.py help`
4. Follow the instructions to use a command. For example, type `python3 cli_application.py find book with title 'Daybreak.'`.

# How to run the test cases
1. Open `cli_application.py` in a text editor and add your MySQL credentials near the top of the file where it says
```
db = mysql.connect(
    host = "marmoset04.shoshin.uwaterloo.ca",
    user = "userid",
    passwd = "pw",
    db = "db356_userid"
)
```
2. Open `cli_test_cases.py` in a text editor and add your MySQL credentials similar to `cli_application.py`
3. Type `python3 cli_test_cases.py` and press enter. If test 15 is reached and no errors are printed, then it is successful.