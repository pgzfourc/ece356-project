import mysql.connector as mysql
from datetime import datetime
import cli_application
import time
import logging

logging.basicConfig(level=logging.DEBUG)

db = mysql.connect(
    host = "marmoset04.shoshin.uwaterloo.ca",
    user = "userid",
    passwd = "pw",
    db = "db356_userid"
)

cursor = db.cursor(buffered=True)

# Test 1: Add book to Inventory
# Successfully add test book
logging.info("[cli_test_cases] Test 1: Add book to Library_Inventory")
bibNum = 0
itemLoc = 'bal'
itemType = 'acbk'
itemCol = 'caaero'
title = 'Test Book'
init_ic = 2
cli_application.run_command(f"add to library inventory {bibNum} '{title}' {itemType} {itemCol} True {itemLoc} {init_ic}")
cursor.execute(f"select count(*) from Library_Inventory where BibNum={bibNum} and ItemType='{itemType}' and ItemCollection='{itemCol}' and ItemLocation='{itemLoc}'")
res = cursor.fetchone()
assert res[0] == 1

# Test 2: borrow_book_by_title
# Book does not exist
logging.info("[cli_test_cases] Test 2: Borrow book by Title: DNE")
res = cli_application.run_command(f"borrow book with title 'This book title should not exist in the table.'")
try:
    assert res == -2
except:
    cli_application.run_command(f"delete from library inventory {bibNum} {itemType} {itemCol} {itemLoc}")
    db.commit()
    assert res == -2

# Borrow a book with ItemCount = 2
logging.info("[cli_test_cases] Test 2: Borrow book by Title: ItemCount 2")
res = cli_application.run_command(f"borrow book with title '{title}'")
db.commit()
cursor.execute(f"select ItemCount from Library_Inventory where \
    BibNum = {bibNum} and \
    ItemType = '{itemType}' and \
    ItemCollection = '{itemCol}' and \
    ItemLocation = '{itemLoc}'")
res2 = cursor.fetchone()
try:
    assert res2[0] == init_ic - 1
except:
    cli_application.run_command(f"delete from library inventory {bibNum} {itemType} {itemCol} {itemLoc}")
    db.commit()
    assert res2[0] == init_ic - 1

# Test 3: Check Availability
# Find available book by title
logging.info("[cli_test_cases] Test 3: Check availability by title")
res = cli_application.run_command(f"find book with title '{title}'")
assert res == init_ic - 1

# Test 4: borrow_books_by_bibnum
# Book does not exist
logging.info("[cli_test_cases] Test 4: Borrow book by Bibnum: DNE")
res = cli_application.run_command("borrow book with bibnum 1")
try:
    assert res == -2
except:
    cli_application.run_command(f"delete from library inventory {bibNum} {itemType} {itemCol} {itemLoc}")
    db.commit()
    assert res == -2

# Borrow a book with ItemCount = 1
logging.info("[cli_test_cases] Test 4: Borrow book by Bibnum: ItemCount 1")
time.sleep(1)
res = cli_application.run_command(f"borrow book with bibnum {bibNum}")
db.commit()
cursor.execute(f"select ItemCount from Library_Inventory where \
    BibNum = {bibNum} and \
    ItemType = '{itemType}' and \
    ItemCollection = '{itemCol}' and \
    ItemLocation = '{itemLoc}' limit 1")
res2 = cursor.fetchone()
try:
    assert res2[0] == init_ic - 2
except:
    cli_application.run_command(f"delete from library inventory {bibNum} {itemType} {itemCol} {itemLoc}")
    db.commit()
    assert res2[0] == init_ic - 2

# Borrow book with title for ItemCount = 0
logging.info("[cli_test_cases] Test 4: Borrow book by Title: ItemCount 0")
res = cli_application.run_command(f"borrow book with title '{title}'")
try:
    assert res == -1
except:
    cli_application.run_command(f"delete from library inventory {bibNum} {itemType} {itemCol} {itemLoc}")
    db.commit()
    assert res == -1


# Borrow book with ItemCount = 0
logging.info("[cli_test_cases] Test 4: Borrow book by Bibnum: ItemCount 0")
res = cli_application.run_command(f"borrow book with bibnum {bibNum}")
try:
   assert res == -1
# Cleanup
except:
    cli_application.run_command(f"delete from library inventory {bibNum} {itemType} {itemCol} {itemLoc}")
    db.commit()
    assert res == -1


# Test 5: Remove book from Inventory
# Successfully remove test book
logging.info("[cli_test_cases] Test 5: Remove test book")
cli_application.run_command(f"delete from library inventory {bibNum} {itemType} {itemCol} {itemLoc}")
db.commit()
cursor.execute(f"select count(*) from Library_Inventory where BibNum = {bibNum} and ItemType='{itemType}' and ItemCollection='{itemCol}' and ItemLocation='{itemLoc}'")
res = cursor.fetchone()
assert res[0] == 0

# Test 6: Add to Books
# Successfully add test book
logging.info("[cli_test_cases] Test 6: Add test book")
cli_application.run_command(f"add to books 0 'Test Book' 1 2021-01-01 'Test Description' 0 aaaaa")
db.commit()
cursor.execute("select count(*) from Books where Id=0")
res = cursor.fetchone()
assert res[0] == 1

# Test 7: Add review
# Success
logging.info("[cli_test_cases] Test 7: Add review")
cli_application.run_command(f"add review 0 1 'it was amazing'")
db.commit()
cursor.execute("select count(*) from User_Ratings where Id = 0")
res = cursor.fetchone()
try:
    assert res[0] == 1
except:
    cli_application.run_command(f"delete from books 0")
    db.commit()
    assert res[0] == 1

# Test 8: Update review
# Success
logging.info("[cli_test_cases] Test 8: Update review")
cli_application.run_command("update review 0 1 'it was ok'")
db.commit()
cursor.execute("select Rating from User_Ratings where Id = 0")
res = cursor.fetchone()
try:
    assert res[0] == 'it was ok'
except:
    cli_application.run_command(f"delete from books 0")
    db.commit()
    assert res[0] == 'it was ok'

# Test 9: Get reviews by book ID
# Success
logging.info("[cli_test_cases] Test 9: Get reviews by book ID")
res = cli_application.run_command(f"find book reviews with id 0")
try:
    assert res[0][0] == 'it was ok'
    assert len(res) == 1
except:
    cli_application.run_command(f"delete from books 0")
    db.commit()
    assert res[0][0] == 'it was ok'
    assert len(res) == 1

# Test 10: Remove from Books
# Successfully remove test book
logging.info("[cli_test_cases] Test 10: Remove test book")
cli_application.run_command(f"delete from books 0")
db.commit()
cursor.execute("select count(*) from Books where Id=0")
res = cursor.fetchone()
assert res[0] == 0

# Test 11: Get info about a Code
# Success
logging.info("[cli_test_cases] Test 11: Get additional code details")
code = 'arbk'
ret = cli_application.run_command(f"find additional details {code}")
cursor.execute(f"select Description, CodeType from Data_Dictionary where Code = '{code}'")
description = cursor.fetchone()
cursor.execute(f"select FormatGroup, FormatSubgroup from Format where Code = '{code}'")
format = cursor.fetchone()
cursor.execute(f"select CategoryGroup, CategorySubgroup from Category where Code = '{code}'")
category = cursor.fetchone()
assert ret[0][0] == code
assert ret[0][1] == description[0]
assert ret[0][2] == description[1]
assert ret[0][3] == format[0]
assert ret[0][4] == format[1]
assert ret[0][5] == category[0]
assert ret[0][6] == category[1]

# Test 12: Get subjects by BibNum
logging.info("[cli_test_cases] Test 12: Get subjects by bibnum")
cursor.execute("select BibNum from Subjects where Subject is not null limit 1")
res = cursor.fetchone()
ret = cli_application.run_command(f"find book subjects {res[0]}")
assert len(ret) > 0

# Test 13: List books by subject
logging.info("[cli_test_cases] Test 13: List books by subject")
subject = "JuvEnile".lower()
ret = cli_application.run_command(f"list books by subject {subject}")
assert subject in ret[0][0].lower()

# Test 14: Check availability of book
logging.info("[cli_test_cases] Test 14: Check book availability by ID")
cursor.execute('select Id from Books INNER JOIN Library_Goodreads using (ISBN) where BibNum is not null')
ids = cursor.fetchone()
if len(ids) == 0:
    logging.error("No valid IDs to test; skipping Test 14...")
else:
    ret = cli_application.run_command(f"find book with id {ids[0]}")
    assert ret > 0

# Test 15: Count books by subject
# Verify count of first subject is correct
logging.info("[cli_test_cases] Test 15: Count books by subject")
ret = cli_application.run_command("count books by subject")
subject = ret[0][0]
cursor.execute(f"select count(Subject) from Subjects where Subject = '{subject}'")
res = cursor.fetchone()
assert res[0] == ret[0][1]

# get ratings for book by id not tested